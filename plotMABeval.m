clear all
clc

% load MABeval.mat
% color1 = 'r';
% color2 = 'k';

% load MABevalExp.mat;
% color1 = 'g';
% color2 = 'b';

load MABevalPerf.mat;
color1 = 'c';
color2 = 'm';

regFrontier(:,1)=[];
regGred(:,1)=[];
roundRegFrontier(:,1) = [];
roundRegGred(:,1) = [];

[meanRFA, sigmaRFA, muciRFA, sigmaciRFA] = normfit(regFrontier);
[meanRGred, sigmaRGred, muciRGred, sigmaciRGred] = normfit(regGred);
[meanRoundRFA, sigmaRoundRFA, muciRoundRFA, sigmaciRoundRFA] = normfit(roundRegFrontier);
[meanRoundRGred, sigmaRoundRGred, muciRoundRGred, sigmaciRoundRGred] = normfit(roundRegGred);

figure(1)
errorbar(1:t_stop,meanRFA,meanRFA-muciRFA(1,:),muciRFA(2,:)-meanRFA,color1),hold on;
errorbar(1:t_stop,meanRGred,meanRGred-muciRGred(1,:),muciRGred(2,:)-meanRGred,color2),hold on;
xlabel('Rounds of play');
ylabel('Normalized accumulated regret');
axis([0 t_stop 0 1]);

figure(2)
errorbar(1:t_stop,meanRoundRFA,meanRoundRFA-muciRoundRFA(1,:),muciRoundRFA(2,:)-meanRoundRFA,color1),hold on;
errorbar(1:t_stop,meanRoundRGred,meanRoundRGred-muciRoundRGred(1,:),muciRoundRGred(2,:)-meanRoundRGred,color2),hold on;
xlabel('Rounds of play');
ylabel('Normalized regret');
axis([0 t_stop 0 1]);

% figure(1)
% legend('FA one-sample est.','e-greedy one-sample est.','FA with p_{sp} est.','e-greedy with p_{sp} est.',...
%        'FA perfect est.','e-greedy perfect est.');
% figure(2)
% legend('FA one-sample est.','e-greedy one-sample est.','FA with p_{sp} est.','e-greedy with p_{sp} est.',...
%        'FA perfect est.','e-greedy perfect est.');
