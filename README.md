### MAB/CSSA ###

Source code for the implementation of the parametric linear programming code for a MAB/superprocess in the context of a CSSA scenario. See http://mariolopezmartinezprojectlog.wordpress.com/

### Branches ###

* master 
      * contains the most stable version of the simulator
* develop
      * for general changes
* feature branches
      * sim_varDataRate: for adding the variable data rate scenario
      * SUsMove: for adding SUs arriving and leaving the system


### Files ###

* MABeval: simulator with no learning
* MABevalExp: with learning
* plotMABeval: for plotting the results