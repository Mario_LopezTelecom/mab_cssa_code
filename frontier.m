function [g,pol]= frontier (rSU,a,delta,alpha,map_s,n)
%Parameters
%a: number of possible offers to the SUs
%delta: discount factor
%alpha: the offers
%map_s: create mapping from (highest_rejected_offer, lowest_accepted_offer) -> index
m = (a*(a+1)*(a+2)/6)+n;                %Number of state-action pairs (not incl. retirement)
                                        %computed with "triangular numbers".
                                        
%map_a; create mapping from (state,action) -> index
map_a = [];
for x_i=1:n   
    hRej = map_s(x_i,1);
    lAcc = map_s(x_i,2); 
    aux_a = [x_i*ones(lAcc-hRej,1) (hRej+1:lAcc)'];
    map_a = [map_a; aux_a];
end
map_a = [map_a; [(1:n)' (a+1).*ones(n,1)] ]; %The mapping of the "retirement" actions

P = zeros(m,n);                     %Transition matrix. Size: m(state-action) x n(state)
c = zeros(m,1);                     %Expected rewards per state-action 
d = [zeros(m-n,1) ; ones(n,1)];     %Retirement reward
sa_i=1;                             %State-action pair iterator
for x_i=1:n                         %State iterator
    hRej = map_s(x_i,1);
    lAcc = map_s(x_i,2);   
    for a_i=hRej+1:lAcc             %Action iterator
        [~, rej_state] = ismember([a_i,lAcc],map_s,'rows'); %If SU reject: hRej(t+1) = a_i
        [~, acc_state] = ismember([hRej,a_i],map_s,'rows'); %If SU accepts: lAcc(t+1) = a_i
        if rej_state ~=0  
            P(sa_i,acc_state) = (a_i-hRej)/(lAcc-hRej);
            P(sa_i,rej_state) = 1-P(sa_i,acc_state);
        else                        %If that (hRej,lAcc) doesn't exist, means that it cannot be rejected
            P(sa_i,x_i) = 1;
        end;
        c(sa_i) = rSU*(1-alpha(a_i))*P(sa_i,acc_state);
        A(:,sa_i) = -delta*P(sa_i,:);
        A(x_i,sa_i) = A(x_i,sa_i)+1;
        sa_i = sa_i+1;
    end;
end
A = [A eye(n)];

%Frontier algorithm                     
B_index = m-n+1:m;      %Index of the variables in the basis (length = n)
nB_index = 1:m-n;       %        ''         not in the basis
B_j = A(:,B_index);     %Initial basis corresponding to the initial policy of retirement
g = zeros(n,1);         %Gittins index of each state

j = 1;
while(true)                                 %State value: lambda_cj+M*lambda_dj
    B_j_transp = B_j';
    lambda_cj = B_j_transp\c(B_index);            %WARNING: these equation systems was not solve correctly
    lambda_dj = B_j_transp\d(B_index);                      %by linsolve because of precision issues      
    c_r = c-A'*lambda_cj;                   %Reduced costs
    d_r = d-A'*lambda_dj;
    if (isempty(find(d_r<0)))
        M(j) = 0;
        break;
    end;
    pivot=-c_r(nB_index)./d_r(nB_index);
    pivot(find(d_r(nB_index)>=0)) = -inf;
    M(j) = max(pivot);
    if M(j)<=0
        M(j) = 0;
        break;
    else
        pivot_index = find(pivot==M(j));
        k = nB_index(pivot_index);             %Index of the state-action candidate pairs to enter the basis
        F = map_a(k,:);                        %State-action candidate pairs to enter the basis
        [F_s, k_index] = unique(F(:,1));       %Make sure the basis has only one action per state
        g(F_s) = max(g(F_s),M(j));
        nB_index(pivot_index(k_index)) = B_index(F_s);
        B_j(:,F_s) = A(:,k(k_index));
        B_index(F_s) = k(k_index);
        j = j+1;
    end
end
pol = map_a(B_index,2);



        
        
        
        
        
        
    
    
    
    
    
    
    
    
    
    



      


        
        