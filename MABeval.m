%MAB/Superprocess and e-greedy simulator
%Scenario: variable data rates, no learning (one sample or perfect estimation)
clear all
close all
clc
InitValues;
global numSU a t_stop max_it delta e N rangeMean_P_sp minMean_P_sp alpha n map_s;
global regFrontier roundRegFrontier regGred roundRegGred g pol max_rCum rCum rCumGred;

for it=1:max_it
    it
    %Other params
    dBmMean_P_sp = rand(1,numSU)*rangeMean_P_sp + minMean_P_sp; %True mean P_sp values (dBm)
    mean_P_sp = 10.^(dBmMean_P_sp/10);                          %in (mW)
    p_sp = exprnd(mean_P_sp);                                   %Sample of P_sp (mW)
    %est_P_sp = p_sp;                                           %Estimation of the power the PU gets (one sample)
    est_P_sp = mean_P_sp;                                       %Perfect ''
    typeSU = randi(a,numSU,1);                                  %Type of each SU / index of lowest accepted offer
    
    %Fixed frontier algorithm policies
    for i=1:numSU
        [g(:,i) pol(:,i)] = frontier(est_P_sp(i),a,delta,alpha,map_s,n); %Gittins index and policy
    end
    
    %Epsilon-greedy initial expectations
    PGred = linspace(1/a,1,a)';                              %Initial prob. of acceptance
    PGred = repmat(PGred,1,numSU);
    est_r = repmat((1-alpha)',1,numSU).*repmat(est_P_sp,a,1); %Estimated rewards for each action and SU
    expected_r = est_r.*PGred;                               %Estimated EXPECTED reward of each SU (changes with the state)
    
    %Simulator
    s_t = ones(numSU,1).*a;                       %FA Current state in time "t". Initially, (0,a) -> a, means (all unknown)
    g_t = g(a,:);
    s_tGred = [zeros(numSU,1) ones(numSU,1).*a];  %Current state for e-greedy. In (hRej,lAcc) format   
    t=1;
    while (t<=t_stop)
        %Each round, the p_sp power at the PU receiver is drawn from a Rayleigh distribution
        p_sp = exprnd(mean_P_sp);                             %Current sample of P_sp
        real_r = repmat((1-alpha)',1,numSU).*repmat(p_sp,a,1); %True rewards for this round
        %MAB
        %Action selection
        [~,SU_t] = max(g_t);            %State with highest Gittins index
        hRej_SU_t = map_s(s_t(SU_t),1); %Index of highest rejected offer
        lAcc_SU_t = map_s(s_t(SU_t),2); %Index of lowest accepted offer
        a_SU_t = pol(s_t(SU_t),SU_t);   %Action suggested by policy for SU_t in its state
        %Obtain reward and update state
        if (typeSU(SU_t)<= a_SU_t)      %If SU_t accepts the offer...
            r = real_r(a_SU_t,SU_t);    %Current reward
            lAcc_SU_t = a_SU_t;         %Update current state with next state
        else                            %If SU_t rejects the offer...
            r = 0;
            hRej_SU_t = a_SU_t;
        end
        [~, s_t(SU_t)] = ismember([hRej_SU_t,lAcc_SU_t],map_s,'rows'); %Update index of next state
        g_t(SU_t) = g(s_t(SU_t),SU_t);
        rCum(t+1) = rCum(t)+r;                      %Accumulated reward of FA. '
        %e-greedy
        %Action selection: chooses the SU-action (SU_tGred,a_SU_tGred) pair with highest expected reward
        if (rand(1,1)>e) 
            [max_r_a,max_a] = max(expected_r);
            [~,SU_tGred] = max(max_r_a);
            a_SU_tGred = max_a(SU_tGred);
        else
            %Randomize SU-action of those unknown
            unkSUa = find(PGred~=0 & PGred~=1);
            if (isempty(unkSUa))               %If evertything is explored, pick the max. exp. reward
                [max_r_a,max_a] = max(expected_r);
                [~,SU_tGred] = max(max_r_a);
                a_SU_tGred = max_a(SU_tGred);
            else
                SUa = unkSUa(randi(length(unkSUa),1));
                [a_SU_tGred,SU_tGred] = ind2sub(size(PGred),SUa);
            end;
        end;
        %Obtain reward and update probabilities and state
        if (typeSU(SU_tGred)<= a_SU_tGred)               %If SU_t accepts the offer...
            rGred = real_r(a_SU_tGred,SU_tGred);         %Current reward
            
            PGred(a_SU_tGred:end,SU_tGred) = 1;
            hRej = s_tGred(SU_tGred,1);
            availA =a_SU_tGred - hRej;
            PGred(hRej+1:a_SU_tGred,SU_tGred) = linspace(1/availA,1,availA);
            s_tGred(SU_tGred,2) = a_SU_tGred;            %Updating l_acc and thus, the state
        else                                             %If SU_t rejects the offer...
            rGred = 0;
            
            PGred(1:a_SU_tGred,SU_tGred) = 0;
            lAcc = s_tGred(SU_tGred,2);
            availA =lAcc - a_SU_tGred;
            PGred(a_SU_tGred+1:lAcc,SU_tGred) = linspace(1/(availA),1,availA);
            
            s_tGred(SU_tGred,1) = a_SU_tGred;
        end
        expected_r(:,SU_tGred) = est_r(:,SU_tGred).*PGred(:,SU_tGred); %Update expected costs
        rCumGred(t+1) = rCumGred(t)+rGred;                             %Accumulated reward for e-greedy
        
        %Genie selecting the best action
        max_rGenie = max(p_sp.*(1-alpha(typeSU)));           %Max. reward in a round round
        max_rCum(t+1) = max_rCum(t)+ max_rGenie;            %Max. possible accumulated reward. t=0 will be deleted
        roundRegFrontier(it,t+1) = 1 - r/max_rGenie;
        roundRegGred(it,t+1) = 1 - rGred/max_rGenie;
        regFrontier(it,t+1) = 1 - rCum(t+1)./max_rCum(t+1);
        regGred(it,t+1) = 1 - rCumGred(t+1)./max_rCum(t+1);
        t=t+1;
    end; 
end;
save MABevalPerf.mat;








        
        

    


    
