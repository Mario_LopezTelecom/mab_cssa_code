clear all
clc

qVal = {'0' '001' '005' '01' '03'};
color = {'b' 'g' 'r' 'c' 'm'};

for ix=1:length(qVal)
    load (['MABevalExp' qVal{ix} '.mat']);
    
    regFrontier(:,1)=[];
    regGred(:,1)=[];
    roundRegFrontier(:,1) = [];
    roundRegGred(:,1) = [];

    [meanRFA, sigmaRFA, muciRFA, sigmaciRFA] = normfit(regFrontier);
    [meanRGred, sigmaRGred, muciRGred, sigmaciRGred] = normfit(regGred);
    [meanRoundRFA, sigmaRoundRFA, muciRoundRFA, sigmaciRoundRFA] = normfit(roundRegFrontier);
    [meanRoundRGred, sigmaRoundRGred, muciRoundRGred, sigmaciRoundRGred] = normfit(roundRegGred);

    figure(1)
    errorbar(1:t_stop,meanRFA,meanRFA-muciRFA(1,:),muciRFA(2,:)-meanRFA,color{ix}),hold on;
    errorbar(1:t_stop,meanRGred,meanRGred-muciRGred(1,:),muciRGred(2,:)-meanRGred,[color{ix} 'o']),hold on;
    xlabel('Rounds of play');
    ylabel('Normalized accumulated regret');
    axis([0 t_stop 0 1]);

    figure(2)
    errorbar(1:t_stop,meanRoundRFA,meanRoundRFA-muciRoundRFA(1,:),muciRoundRFA(2,:)-meanRoundRFA,color{ix}),hold on;
    errorbar(1:t_stop,meanRoundRGred,meanRoundRGred-muciRoundRGred(1,:),muciRoundRGred(2,:)-meanRoundRGred,[color{ix} 'o']),hold on;
    xlabel('Rounds of play');
    ylabel('Normalized regret');
    axis([0 t_stop 0 1]);
end;
