%MAB/Superprocess and e-greedy simulator
%Scenario: learning variable data rates.
clear all
close all
clc

initValues;
global numSU a t_stop max_it delta e N rangeMean_P_sp minMean_P_sp alpha n map_s;
global regFrontier roundRegFrontier regGred roundRegGred g pol max_rCum rCum rCumGred;
global p q;

for it=1:max_it
    it
    %Other params
    dBmMean_P_sp = rand(1,numSU)*rangeMean_P_sp + minMean_P_sp; %True mean P_sp values (dBm)
    mean_P_sp = 10.^(dBmMean_P_sp/10);                          %in (mW)
    p_sp = exprnd(mean_P_sp);                                   %Samples of P_sp, one for each SU (mW)                                       %Estimation of the mean of P_sp (with one sample)
    est_rSU = log2(1+p_sp./N);                                  %Estimation of the data rate the PU gets (one sample)
    typeSU = randi(a,numSU,1);                                  %Type of each SU / index of lowest accepted offer                                    
   
    %Frontier algorithm initial beliefs (initially the same than e-greedy)
    FAest_rSU = est_rSU;                                        %FA estimated data rate
    FAa_gamma = ones(1,numSU);
    FAb_gamma = p_sp;
    %Frontier algorithm policies
    for i=1:numSU
        [g(:,i) pol(:,i)] = frontier(FAest_rSU(i),a,delta,alpha,map_s,n);
    end    
    g_t = g(a,:);
    
    %Epsilon-greedy initial expectations and beliefs
    gredEst_rSU = est_rSU;                                        %e-greedy estimated data rate 
    gredA_gamma = FAa_gamma;
    gredB_gamma = FAb_gamma;
    PGred = linspace(1/a,1,a)';                                   %Initial prob. of acceptance
    PGred = repmat(PGred,1,numSU);
    est_r = repmat((1-alpha)',1,numSU).*repmat(gredEst_rSU,a,1);  %Estimated rewards for each action and SU
    expected_r = est_r.*PGred;                                    %Estimated EXPECTED reward of each SU (changes with the state)
    
    %Simulator
    s_t = ones(numSU,1).*a;                       %Current state in time "t". Initially, (0,a) -> a, means (all unknown)    
    s_tGred = [zeros(numSU,1) ones(numSU,1).*a];  %Current state for e-greedy. In (hRej,lAcc) format
    t=1;
    while (t<=t_stop)
        if (rand <= q && numSU ~= 1) %SU departure. [TO-DO]: convert SUs to objects, to avoid the following... ->
            leavSU = randi(numSU,1); %Leaving SU
            typeSU(leavSU) = [];
            mean_P_sp(leavSU) = [];
            FAest_rSU(leavSU) = [];
            g(:,leavSU) = [];
            g_t(leavSU) = [];
            pol(:,leavSU) = [];
            s_t(leavSU) = [];
            s_tGred(leavSU,:) = [];
            FAa_gamma(leavSU) = [];
            FAb_gamma(leavSU) = [];
            gredA_gamma(leavSU) = [];
            gredB_gamma(leavSU) = [];
            gredEst_rSU(leavSU) = [];            
            est_r(:,leavSU) = [];
            PGred(:,leavSU) = [];
            expected_r(:,leavSU) = [];
            numSU = numSU - 1; 
        end;
        if (rand <= p && numSU <= 15) %SU arrival       
            typeSU = [typeSU; randi(a)];
                newDBmMean_P_sp = rand*rangeMean_P_sp + minMean_P_sp; 
                newMean_P_sp = 10.^(newDBmMean_P_sp/10);
            mean_P_sp = [mean_P_sp newMean_P_sp];
                newP_sp = exprnd(newMean_P_sp); 
                newFAest_rSU = log2(1+newP_sp/N); 
            FAest_rSU = [FAest_rSU newFAest_rSU];
                [newG newPol] = frontier(newFAest_rSU,a,delta,alpha,map_s,n);
            g = [g newG];
            g_t = [g_t g(a,end)];
            pol = [pol newPol];
            s_t = [s_t; a];
            s_tGred = [s_tGred; [0 a]];
            FAa_gamma = [FAa_gamma 1];
            FAb_gamma = [FAb_gamma newP_sp];
            gredA_gamma = [gredA_gamma 1];
            gredB_gamma = [gredB_gamma newP_sp];
                newGredEst_rSU = newFAest_rSU;
            gredEst_rSU = [gredEst_rSU newGredEst_rSU];               
                newEst_r = (1-alpha).*newGredEst_rSU;
            est_r = [est_r newEst_r'];
                newPGred = linspace(1/a,1,a);
            PGred = [PGred newPGred'];
            expected_r = [expected_r (newEst_r.*newPGred)'];
            numSU = numSU + 1;
        end;
        %Each round, the p_sp power at the PU receiver is drawn from a Rayleigh distribution
        p_sp = exprnd(mean_P_sp);                              %Current sample of P_sp
        rSU = log2(1+p_sp/N);                                  %Current sample of rSU
        real_r = repmat((1-alpha)',1,numSU).*repmat(rSU,a,1);  %True rewards for this round
        
        %MAB
        %Action selection
        [~,SU_t] = max(g_t);                        %State with highest Gittins index
        hRej_SU_t = map_s(s_t(SU_t),1);             %Index of highest rejected offer
        lAcc_SU_t = map_s(s_t(SU_t),2);             %Index of lowest accepted offer
        a_SU_t = pol(s_t(SU_t),SU_t);               %Action suggested by policy for SU_t in its state
        %Obtain reward and update state
        if (typeSU(SU_t)<= a_SU_t)                  %If SU_t accepts the offer...
            r = real_r(a_SU_t,SU_t);                %Current reward
            lAcc_SU_t = a_SU_t;                     %Update current state with next state
            %Update belief of p_sp
            FAa_gamma(SU_t) = FAa_gamma(SU_t)+1;
            FAb_gamma(SU_t) = FAb_gamma(SU_t)+p_sp(SU_t);
            FAestMean_P_sp_SU_t = FAb_gamma(SU_t)/FAa_gamma(SU_t);
            FAest_rSU(SU_t) = log2(1+FAestMean_P_sp_SU_t/N); 
            %Update Gittins index
            [g(:,SU_t) pol(:,SU_t)] = frontier(FAest_rSU(SU_t), a, delta,alpha,map_s,n);
        else                                        %If SU_t rejects the offer...
            r = 0;
            hRej_SU_t = a_SU_t;
        end
        [~, s_t(SU_t)] = ismember([hRej_SU_t,lAcc_SU_t],map_s,'rows'); %Update index of next state
        g_t(SU_t) = g(s_t(SU_t),SU_t);
        rCum(t+1) = rCum(t)+r;
        
        %e-greedy
        %Action selection: chooses the SU-action (SU_tGred,a_SU_tGred) pair with highest expected reward
        if (rand(1,1)>e) 
            [max_r_a,max_a] = max(expected_r);
            [~,SU_tGred] = max(max_r_a);
            a_SU_tGred = max_a(SU_tGred);
        else
            %Randomize SU-action of those unknown
            unkSUa = find(PGred~=0 & PGred~=1);
            if (isempty(unkSUa))               %If evertything is explored, pick the max. exp. reward
                [max_r_a,max_a] = max(expected_r);
                [~,SU_tGred] = max(max_r_a);
                a_SU_tGred = max_a(SU_tGred);
            else
                SUa = unkSUa(randi(length(unkSUa),1));
                [a_SU_tGred,SU_tGred] = ind2sub(size(PGred),SUa);
            end;
        end;
        %Obtain reward and update probabilities and state
        if (typeSU(SU_tGred)<= a_SU_tGred)               %If SU_t accepts the offer...
            rGred = real_r(a_SU_tGred,SU_tGred);         %Current reward
            
            PGred(a_SU_tGred:end,SU_tGred) = 1;
            hRej = s_tGred(SU_tGred,1);
            availA =a_SU_tGred - hRej;
            PGred(hRej+1:a_SU_tGred,SU_tGred) = linspace(1/availA,1,availA);
            
            s_tGred(SU_tGred,2) = a_SU_tGred;            %Updating l_acc and thus, the state
            
            gredA_gamma(SU_tGred) = gredA_gamma(SU_tGred)+1;
            gredB_gamma(SU_tGred) = gredB_gamma(SU_tGred)+p_sp(SU_tGred);
            gredEstMean_P_sp_SU_t = gredB_gamma(SU_tGred)/gredA_gamma(SU_tGred);
            gredEst_rSU(SU_tGred) = log2(1+gredEstMean_P_sp_SU_t/N); 
            est_r(:,SU_tGred) = gredEst_rSU(SU_tGred).*(1-alpha);
        else                                             %If SU_t rejects the offer...
            rGred = 0;
            
            PGred(1:a_SU_tGred,SU_tGred) = 0;
            lAcc = s_tGred(SU_tGred,2);
            availA =lAcc - a_SU_tGred;
            PGred(a_SU_tGred+1:lAcc,SU_tGred) = linspace(1/(availA),1,availA);
            
            s_tGred(SU_tGred,1) = a_SU_tGred;
        end
        expected_r(:,SU_tGred) = est_r(:,SU_tGred).*PGred(:,SU_tGred); %Update expected costs
        rCumGred(t+1) = rCumGred(t)+rGred;
        
        %Genie selecting the best action
        max_rGenie = max(rSU.*(1-alpha(typeSU)));
        max_rCum(t+1) = max_rCum(t)+ max_rGenie;
        roundRegFrontier(it,t+1) = 1 - r/max_rGenie;
        roundRegGred(it,t+1) = 1 - rGred/max_rGenie;
        regFrontier(it,t+1) = 1 - rCum(t+1)./max_rCum(t+1);
        regGred(it,t+1) = 1 - rCumGred(t+1)./max_rCum(t+1);
        t=t+1;
    end; 
end;
save MABevalExp.mat;
