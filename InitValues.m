function [] = InitValues
%Common parameters to all simulations

global numSU a t_stop max_it delta e N rangeMean_P_sp minMean_P_sp alpha n map_s;
global regFrontier roundRegFrontier regGred roundRegGred g pol max_rCum rCum rCumGred;
global p q;

%Inputs
numSU = 10;                     %Number of SUs
a = 10;                         %Number of possible offers to the SUs

t_stop = 100;                   %Number of slots to simulate
max_it = 100;                     %Number of experiments

delta = 0.99;                   %Discount factor
e = 0.3;                        %Prob of random action of the epsilon-greedy alg.

dBm_N = -103;                   %Noise power in dBm
maxMean_P_sp = -60;             %See 'rangeMean_P_sp'
minMean_P_sp = -90;

p = 0;                          %Prob. of a SU arrival     
q = 0;                          %Prob. of a SU departure

%Other params
N = 10^(dBm_N/10);                              %Noise power in mW
rangeMean_P_sp = maxMean_P_sp - minMean_P_sp;   %Range of the true mean powers from each SU...
                                                %...at the PU receiver

alpha = linspace(1/(a+1),...    %Possible time '%' offered to SUs
                 a/(a+1),...
                 a);
n = a*(a+1)/2;                  %Number of knowledge states

%Indexing of knowledge states (hRej,lAcc) -> index
map_s = [];
for i=0:a-1
    aux = [i*ones(a-i,1) (i+1:a)'];
    map_s = [map_s; aux];
end;

%Pre-allocation for speed
regFrontier = zeros(max_it,t_stop+1);
roundRegFrontier = zeros(max_it,t_stop+1);
regGred = zeros(max_it,t_stop+1);
roundRegGred = zeros(max_it,t_stop+1);
g = zeros(n,numSU);     
pol = zeros(n,numSU);   
max_rCum = zeros(1,t_stop+1);  
rCum = zeros(1,t_stop+1);      
rCumGred = zeros(1,t_stop+1);